#!/usr/bin/env bash

# Capture the output of lspci in a variable, removing newlines
lspci_output="$(lspci -k | grep VGA | tr -d '\n')"

VENDOR="$(cat /sys/devices/virtual/dmi/id/board_vendor)"
VEN_ID="$(cat /sys/devices/virtual/dmi/id/chassis_vendor)"

install_packages() {
    pacman -S --needed --noconfirm "$@"
}

nvidia_check_arch () {
    if echo "$lspci_output" | grep -i "nvidia" ; then
        # Tesla
        [[ "$lspci_output" == *G[0-9]* ]] || [[ "$lspci_output" == *GT[0-9]* ]] || [[ "$lspci_output" == *MCP[0-9]* ]] && nv_arch=Tesla
        # Fermi
        [[ "$lspci_output" == *GF[0-9]* ]] && nv_arch=Fermi
        # Kepler
        [[ "$lspci_output" == *GK[0-9]* ]] && nv_arch=Kepler
    fi
}

nvidia_check_arch

# Install CPU Microcode
cpu_model_name=$(grep -m 1 -oP '(?<=vendor_id\s:\s)\S+' /proc/cpuinfo)

if [ "$cpu_model_name" = "GenuineIntel" ]
then install_packages intel-ucode
fi

if [ "$cpu_model_name" = "AuthenticAMD" ]
then install_packages amd-ucode
fi

# Check for Nvidia GPU and install appropriate driver
 if echo "$lspci_output" | grep -i "nvidia"; then
    if [ "$nv_arch" == "Fermi" ]; then
        install_packages nvidia-390xx-meta
    elif [ "$nv_arch" == "Kepler" ]; then
        install_packages nvidia-470xx-meta
    elif [ "$nv_arch" == "Tesla" ]; then
        install_packages mesa lib32-mesa nouveau-reclocking nouveau-fw
        if [[ $(nouveau-reclocking --list) ]] ; then
        	nouveau-reclocking --max --save
        fi
    else
        install_packages nvidia-meta
    fi
fi

# Check for available GPU modules and install appropriate drivers
if [ -d /sys/module/amdgpu ] || [ -d /sys/module/radeon ]; then
    install_packages amd-meta
fi

if [ -d /sys/module/i915 ] ; then
    install_packages intel-meta
fi

# Install SteamDeck packages if needed
if [[ ":Valve:" =~ :"$VENDOR": ]] && ! [[ $(pacman -Qqs linux-fsync-nobara) ]]; then
    install_packages steamdeck-dkms
fi

# Install HHD=UI packages if needed
if ! [[ ":Valve:" =~ :"$VENDOR": ]] && [[ $(pacman -Qqs lunaos-settings-handheld) ]]; then
    install_packages hhd-ui
fi

# Install GPG drivers if needed
if [[ ":GPD:" =~ :"$VENDOR": ]]; then
    install_packages gpd-fan-driver-dkms-git
fi

# Install AYANEO drivers if needed
if [[ ":AYADEVICE:AYA NEO:AYANEO:" =~ :"$VENDOR": ]]; then
    install_packages ayaneo-platform-dkms-git ayn-platform-dkms-git
fi

# Install surface packages if needed
if [[ ":Microsoft:" =~ ":$VEN_ID:" ]]; then
	# libwacom-surface conflict with libwacom
	yes | pacman -S libwacom-surface surface-ipts-firmware surface-dtx-daemon-bin surface-ath10k-firmware-override linux-firmware-marvell
fi

# Install xbox packages if needed
if [[ $(pacman -Qqs xone-dkms-git) ]] && ! [[ $(pacman -Qqs linux-fsync-nobara) ]]; then
    install_packages xpadneo-dkms
fi

