# SPDX-FileCopyrightText: no
# SPDX-License-Identifier: CC0-1.0
#
# Configuration for LunaOS Desktop chooser in Calamares

mode: required

method: netinstall-select

labels:
    step: "Desktop"
    step[de]: "Desktop"
    step[fi]: "Työpöytä"
    step[fr]: "Bureau"
    step[it]: "Desktop"
    step[es]: "Escritorio"
    step[ru]: "Рабочий стол"
    step[zh_CN]: "桌面"
    step[ja]: "デスクトップ"
    step[sv]: "Skrivbord"
    step[pt_BR]: "Ambiente de Trabalho"
    step[tr]: "Masaüstü"
    step[ro]: "Spațiu de lucru"

default: GNOME-Desktop

items:

    - id: ""
      name: "No Desktop"
      description: "Please pick one of the Desktops from the list. If you don't want to install any Desktop, that's fine, your system will start up in text-only mode and you can install a desktop environment"
      description[ru]: "Пожалуйста, выберите один из рабочих столов из списка. Если вы не хотите устанавливать ни один рабочий стол, это не страшно,ваша система будет запущена в текстовом режиме, и вы сможете установить окружение рабочего стола позже."
      screenshot: "/etc/calamares/images/no.png"

    - id: KDE-Desktop
      name: "KDE Plasma"
      description: "Use Plasma to surf the web; keep in touch with colleagues, friends and family; manage your files, enjoy music and videos; and get creative and productive at work. Do it all in a beautiful environment that adapts to your needs, and with the safety, privacy-protection and peace of mind that the best Free Open Source Software has to offer."
      description[de]: "Nutze Plasma, um im Internet zu surfen, mit Kollegen, Freunden und Familie in Kontakt zu bleiben, Verwalte deine Dateien, genieße Musik und Video und sei kreativ und produktiv bei der Arbeit. Tu das alles in einer schönen Umgebung, die sich Deinen Bedürfnissen anpasst, und mit der Sicherheit, dem Schutz der Privatsphäre und dem Seelenfrieden, den die beste Freie Open-Source-Software zu bieten hat."
      description[fi]: "Plasman avulla voit surffata verkossa, pitää yhteyttä kollegoihin, ystäviin ja perheeseen, hallita tiedostoja, nauttia musiikista ja videoista sekä olla luova ja tuottava työssäsi. Tee kaikki tämä kauniissa ympäristössä, joka mukautuu tarpeisiisi, ja nauti turvallisuudesta, yksityisyydensuojasta ja mielenrauhasta, joita parhaat vapaat avoimen lähdekoodin ohjelmistot voivat tarjota."
      description[fr]: "Utilisez Plasma pour surfer sur le web, rester en contact avec ses collègues, ses amis et sa famille, manager ses fichiers, profiter de sa musique et de ses vidéos et être créatif et productif au travail. Faites tout cela dans un bel environnement s’adaptant à vos besoins et vous offrant une meilleure sécurité, la protection de votre vie privé et une tranquillité d’esprit."
      description[it]: "Usa Plasma per navigare sul web; tieniti in contatto con colleghi, amici e famiglia; gestisci i tuoi file, goditi musica e video; e diventa creativo e produttivo al lavoro. Fai tutto questo in un bellissimo ambiente che si adatta alle tue esigenze, e con la sicurezza, la protezione della privacy e la tranquillità che il miglior software libero open source ha da offrire."
      description[es]: "Use Plasma para navegar por la web; manténgase en contacto con sus colegas, amigos y familiares; administre sus archivos, disfrute de música y vídeos; y sea creativo y productivo durante el trabajo. Haga todo esto en un agradable entorno que se adapta a sus necesidades con seguridad, protección de su privacidad y con la tranquilidad que le proporciona el mejor software libre de código abierto."
      description[ru]: "Воспользуйтесь Plasma для просмотра интернета, держите связь с коллегами, друзьями и семьей, управляйте вашими файлами, наслаждайтесь музыкой и видео и будьте творческими и продуктивным на работе. Делайте все это в красивой среде, которая адаптируется к вашим потребностям, и все это безопасно, с защитой конфиденциальности и спокойствием, которое может предоставить лучшее свободное программное обеспечение с открытым исходным кодом."
      description[zh_CN]: "您可以使用 Plasma 桌面环境轻松浏览网页，与同事、朋友和家人保持联系，管理文件，欣赏音乐和视频，并在工作中发挥创意和提高效率。 Plasma 是自由开源软件界的明星产品，它的界面美观、安全可靠、尊重隐私且可按需定制，让您可以安心愉悦地每日使用。"
      description[ja]: "Plasma を使ってネットサーフィンをしたり、同僚や友人、家族と連絡を取ったり、ファイルを管理したり、音楽やビデオを楽しんだり、仕事で創造性や生産性を発揮することができます。そのすべてを、あなたのニーズに合わせた美しい環境と、最高のフリー・オープンソース・ソフトウェアが提供する安全性、プライバシー保護、安心感の中で行うことができるのです。"
      description[sv]: "Använd Plasma, för att surfa på webben, hålla kontakten med kollegor, vänner och familj; hantera dina filer, njut av musik och filmer; och var kreativ och produktiv på jobbet. Gör allt i en vacker miljö som anpassar sig efter dina behov och med den säkerhet, integritetsskydd och sinnesfrid som den bästa gratis programvaran med öppen källkod har att erbjuda."
      description[pt_BR]: "Use o Plasma para navegar pela Web; esteja em contacto com os seus colegas, amigos e família; faça a gestão dos seus ficheiros, desfrute da música e dos vídeos; seja criativo e produtivo no seu trabalho. Faça tudo isso num ambiente de trabalho bonito que se adapta às suas necessidades e com a segurança, protecção de privacidade e paz de espírito que o melhor ‘Software’ Livre tem para oferecer."
      description[tr]: "Plasma'yı internette gezinmek; iş arkadaşlarınız, dostlarınız ve ailenizle iletişimde kalmak; dosyalarınızı yönetmek, müzik ve videoların tadını çıkarmak; işte yaratıcı ve üretken olmak için kullanın. Tüm bunları ihtiyaçlarınıza uygun güzel bir çalışma ortamında ve en iyi Özgür Yazılımın sunduğu güvenlik, gizlilik koruması ve gönül rahatlığıyla yapın."
      description[ro]: "Folosiți Plasma pentru a naviga pe internet, pentru a păstra legătura cu colegii, prietenii și familia, pentru a vă gestiona fișierele, pentru a vă bucura de muzică și videoclipuri și pentru a fi creativi și productivi la locul de muncă. Faceți toate acestea într-un mediu frumos care se adaptează la nevoile dvs. și cu siguranța, protecția confidențialității și liniștea pe care o oferă cel mai bun software liber cu sursă deschisă."
      screenshot: "/etc/calamares/images/plasma.png"

    - id: GNOME-Desktop
      name: "GNOME"
      description: "Get things done with ease, comfort, and control. An easy and elegant way to use your computer, GNOME is designed to help you have the best possible computing experience."
      description[de]: "Erledigen Sie Ihre Aufgaben mit Leichtigkeit, Komfort und Kontrolle. GNOME ist eine einfache und elegante Art und Weise, Ihren Computer zu benutzen, und wurde entwickelt, um Ihnen das bestmögliche Computererlebnis zu bieten."
      description[fi]: "Hoida asiat helposti, mukavasti ja hallitusti. GNOME on helppo ja tyylikäs tapa käyttää tietokonetta, ja se on suunniteltu auttamaan sinua saamaan parhaan mahdollisen tietokonekokemuksen."
      description[fr]: "Réalisez vos tâches avec facilité, confort et contrôle. Une façon simple et élégante d'utiliser votre ordinateur, GNOME est conçu pour vous aider à avoir la meilleure expérience informatique possible."
      description[it]: "Fai le cose con facilità, comodità e controllo. Un modo semplice ed elegante di usare il tuo computer, GNOME è progettato per aiutarti ad avere la migliore esperienza informatica possibile."
      description[es]: "Haga las cosas con facilidad, comodidad y control. Una forma fácil y elegante de usar su ordenador, GNOME está diseñado para ayudarle a tener la mejor experiencia informática posible."
      description[ru]: "Делайте все с легкостью, комфортом и контролем. Простой и элегантный способ использования компьютера, GNOME создан для того, чтобы помочь вам получить наилучшие впечатления от работы на компьютере."
      description[zh_CN]: "以轻松、舒适和控制的方式完成事情。作为使用计算机的一种简单而优雅的方式，GNOME被设计用来帮助您获得最佳的计算体验。"
      description[ja]: "簡単で、快適で、コントロールしやすい。コンピュータを使うための簡単でエレガントな方法である GNOME は、あなたが可能な限り最高のコンピュータ体験をできるように設計されています。"
      description[sv]: "Få saker gjorda med lätthet, komfort och kontroll.. GNOME är ett enkelt och elegant sätt att använda din dator, GNOME är designat för att hjälpa dig att få bästa möjliga datorupplevelse."
      description[pt_BR]: "Faça as coisas com facilidade, conforto e controle. Uma maneira fácil e elegante de usar seu computador, GNOME foi projetado para te ajudar a ter a melhor experiência computacional possível."
      description[tr]: "İşlerinizi kolay, konforlu ve kontrollü bir şekilde halledin. Bilgisayarınızı kullanmanın kolay ve zarif bir yolu olan GNOME, mümkün olan en iyi bilgisayar deneyimini yaşamanıza yardımcı olmak için tasarlanmıştır."
      description[ro]: "Faceți lucrurile cu ușurință, confort și control. Un mod simplu și elegant de a vă utiliza computerul, GNOME este conceput pentru a vă ajuta să aveți cea mai bună experiență de utilizare a calculatorului."
      screenshot: "/etc/calamares/images/gnome.png"
