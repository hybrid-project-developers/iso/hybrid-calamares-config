#!/usr/bin/env python3

import os
import subprocess
import shutil

import libcalamares
from libcalamares.utils import gettext_path, gettext_languages

import gettext

_translation = gettext.translation("calamares-python",
                                   localedir=gettext_path(),
                                   languages=gettext_languages(),
                                   fallback=True)
_ = _translation.gettext
_n = _translation.ngettext

custom_status_message = None
name = "Prepare for bootloader"
user_output = False


def pretty_name():
    return _(name)


def pretty_status_message():
    if custom_status_message is not None:
        return custom_status_message

    try:
        installation_root_path = libcalamares.globalstorage.value("rootMountPoint")
    except KeyError:
        libcalamares.utils.warning('Global storage value "rootMountPoint" missing')


def run_dracut(installation_root_path):
    kernel_search_path = "/usr/lib/modules"

    # find all the installed kernels and run dracut
    for root, dirs, files in os.walk(os.path.join(installation_root_path, kernel_search_path.lstrip('/'))):
        for file in files:
            if file == "pkgbase":
                kernel_version = os.path.basename(root)
                # run dracut
                pkgbase_location = os.path.join(root, file)
                with open(pkgbase_location, 'r') as pkgbase_file:
                    kernel_suffix = pkgbase_file.read().rstrip()
                try:
                    libcalamares.utils.target_env_process_output(["dracut", "--force", "--hostonly",
                                                                  "--no-hostonly-cmdline",
                                                                  f"/boot/initramfs-{kernel_suffix}.img",
                                                                  kernel_version])
                    libcalamares.utils.target_env_process_output(["dracut", "--force", "--no-hostonly",
                                                                  f"/boot/initramfs-{kernel_suffix}-fallback.img",
                                                                  kernel_version])
                except subprocess.CalledProcessError as cpe:
                    libcalamares.utils.warning(f"dracut failed with error: {cpe.stderr}")

                kernel_name = f"vmlinuz-{kernel_suffix}"
                # copy kernel to boot
                shutil.copy2(os.path.join(root, "vmlinuz"), os.path.join(installation_root_path, "boot", kernel_name))


def run():
    if not libcalamares.job.configuration:
        return "No configuration found", "Aborting due to missing configuration"

    try:
        gs_name = libcalamares.job.configuration["gsName"]
    except KeyError:
        return "Missing  value", "gsname not found in configuration file"

    bootloaders = libcalamares.job.configuration.get("bootloader", [])

    try:
        installation_root_path = libcalamares.globalstorage.value("rootMountPoint")
    except KeyError:
        libcalamares.utils.warning('Global storage value "rootMountPoint" missing')

    packages = None

    for bootloader in bootloaders:
        try:
            if bootloader["name"].casefold() == gs_name.casefold():
                packages = bootloader["packages"]
        except KeyError:
            return f"Configuration error", f"Missing key 'name' in configuration"

    # install packages
    if packages is not None:
        try:
            curr_filesystem = subprocess.run(["findmnt", "-ln", "-o", "FSTYPE", installation_root_path], stdout=subprocess.PIPE).stdout.decode('utf-8')
            libcalamares.utils.debug("Current filesystem: {!s}".format(curr_filesystem))
            is_root_on_btrfs = (curr_filesystem == "btrfs\n")
            if is_root_on_btrfs and gs_name.casefold().strip() == "grub":
                libcalamares.utils.debug("Root on BTRFS")
                packages += ["grub-btrfs"]
            libcalamares.utils.target_env_process_output(["pacman", "--noconfirm", "-S"] + packages)
        except subprocess.CalledProcessError as cpe:
            return f"Failed to install packages for {gs_name}", f"The install failed with error: {cpe.stderr}"

    # Run dracut unless we are using systemd-boot since kernel-install handles that
    if gs_name.casefold().strip() != "systemd-boot":
        run_dracut(installation_root_path)

    return None
